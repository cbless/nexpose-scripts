#!/usr/bin/env python

from __future__ import print_function
import argparse
import json
import re
from termcolor import colored

import requests
from requests.auth import HTTPBasicAuth
from requests.packages.urllib3.exceptions import InsecureRequestWarning


REPORTS_URL_TEMPLATE = "{0}:{1}/api/3/reports"
REPORTS_URL_ALL_TEMPLATE = "{0}:{1}/api/3/reports?page=0&size={2}&sort=id,asc"
REPORT_DOWNLOAD_LINK_TEMPLATE = "{0}/history/latest/output"


def main():
    parser = argparse.ArgumentParser(description="Nexpose Report Downloader")
    parser.add_argument("--url", metavar="URL", type=str, required=False, default="https://localhost")
    parser.add_argument("--port", metavar="PORT", type=int, default=3780, help="The port number of the nexpose instance")
    parser.add_argument("--insecure", required=False, action='store_true', help="Disable the validation of TLS/SSL certificates")
    parser.add_argument("-u", "--user", metavar="USER", type=str, default="root", required=False, help="User account to use for authentication")
    parser.add_argument("-p", "--password", metavar="PASSWORD", type=str, required=True, help="Password for the given user account")
    parser.add_argument("-r", "--report", metavar="REPORT", type=str, required=False, default="", help="report name or regex")
    args = parser.parse_args()

    reports_url = REPORTS_URL_TEMPLATE.format(args.url, args.port)
    verify = (not args.insecure)
    if not verify:
        requests.packages.urllib3.disable_warnings(InsecureRequestWarning)

    try:
        reports_response = requests.get(reports_url, auth=HTTPBasicAuth(args.user, args.password), verify=verify)
        reports1 = json.loads(reports_response.content)

        count = reports1['page']['totalResources']
        reports_url_all = REPORTS_URL_ALL_TEMPLATE.format(args.url, args.port, count)

        reports_response = requests.get(reports_url_all, auth=HTTPBasicAuth(args.user, args.password), verify=verify)
        reports2 = json.loads(reports_response.content)

        print (reports1['page']['totalResources'])
        for report in reports2['resources']:
            report_format = report['format']
            report_name = report['name']
            report_link = ""

            matched = False
            if not re.match(args.report, report_name):
                continue
            for link in report['links']:
                if link['rel'] == 'self':
                    report_link = REPORT_DOWNLOAD_LINK_TEMPLATE.format(link['href'])

            outfile = "{0}.{1}".format(report_name,report_format)
            print (colored("[+] Downloading report {0}".format(outfile), "green"))

            report_response = requests.get(report_link, auth=HTTPBasicAuth(args.user, args.password), verify=verify)
            with open(outfile, "wb") as report_file:
                report_file.write(report_response.content)

    except requests.exceptions.SSLError:
        print(colored("Insecure or not a valid TLS/SSL certificate", "red"))


if __name__=="__main__":
    main()

